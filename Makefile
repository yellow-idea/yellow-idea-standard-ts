build: bd

bd:
	firebase use default && firebase deploy

bd2:
	firebase use prod && firebase deploy

bdf:
	firebase use default && firebase deploy --only functions:$(name)

bdf2:
	firebase use prod && firebase deploy --only functions:$(name)

start:
	cd functions && npm run build && firebase emulators:start --only functions
