const express = require('express');
const cors = require('cors');
const server = express();

server.use(cors());
// @ts-ignore
server.use(express.urlencoded({limit: '50mb', extended: true, strict: false}));
server.use(express.json({limit: '50mb'}));

server.get('/', (req: any, res: { send: (arg0: string) => void; }) => {
    console.log(process.env)
    res.send('Hello xx' + process.env.ENV + " " + process.env.FIREBASE_CONFIG)
})

export default server;
