import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import {helperEnv} from '../../helper/env'
import AppAPI from './api_app'
import CmsAPI from './api_cms'

admin.initializeApp()

const app = functions.region('asia-east2').https.onRequest(helperEnv(AppAPI))
const cms = functions.region('asia-east2').https.onRequest(helperEnv(CmsAPI))

export default {
    app,
    cms
}
