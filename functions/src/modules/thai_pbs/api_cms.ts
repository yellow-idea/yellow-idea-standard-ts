const express = require('express');
const cors = require('cors');
const server = express();

server.use(cors());
// @ts-ignore
server.use(express.urlencoded({limit: '50mb', extended: true, strict: false}));
server.use(express.json({limit: '50mb'}));

export default server;
