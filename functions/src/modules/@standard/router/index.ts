import * as express from 'express';

const app = express();

let count = 0;

app.get('/cat', (request: any, response: { send: (arg0: string) => void; }) => {
    count++;
    console.log(1234)
    console.info(`Info`)
    console.warn(`Warn`)
    console.error(`Error`)
    response.send('CAT ' + `${count} ` + process.env.NODE_ENV);
});

app.get('/dog', (request: any, response: { send: (arg0: string) => void; }) => {
    response.send('DOG' + `${count} `);
});

export default app;
