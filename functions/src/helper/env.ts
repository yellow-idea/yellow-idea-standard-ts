export const helperEnv = (server: (arg0: any, arg1: any) => any) => {
    return (request: any, response: any) => {
        if (!process.env.ENV) {
            console.log("First Time")
            if (process.env.GCLOUD_PROJECT === "yellow-idea-dev") {
                process.env.ENV = "dev"
            } else {
                process.env.ENV = "prod"
            }
        } else {
            console.log("Next Time")
        }
        return server(request, response)
    }
}
