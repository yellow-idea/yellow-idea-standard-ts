import axios from 'axios'

const LINE_URL_PUSH_MESSAGE: string = 'https://api.line.me/v2/bot/message/push'

export interface iLinePushMessage {
    status: boolean
    data: any
}

export const LinePushMessage = async (token: string, to: any, messages: Array<any>): Promise<iLinePushMessage> => {
    try {
        const {status, data} = await axios({
            method: 'post',
            url: LINE_URL_PUSH_MESSAGE,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            data: {to, messages}
        })
        return {status: status === 200, data}
    } catch (e) {
        return {status: false, data: e}
    }
}
