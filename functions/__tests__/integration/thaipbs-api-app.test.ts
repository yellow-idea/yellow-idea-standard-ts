// import * as functions from 'firebase-functions-test';
// import * as admin from 'firebase-admin';
//
// const testEnv = functions();
// import {app} from '../../src/modules/thai_pbs';

// const test = require('firebase-functions-test')({
//     databaseURL: 'https://my-project.firebaseio.com',
//     storageBucket: 'my-project.appspot.com',
//     projectId: 'my-project',
// }, 'path/to/serviceAccountKey.json');

import server from '../../src/modules/thai_pbs/api_app'

const supertest = require('supertest')
const request = supertest.agent(server)

describe('General', () => {
    test('Enter home page', (done) => {
        request.get('/')
            .expect(200)
            .end((err: any, res: any) => {
                if (err) throw err
                let result = res.res.text
                expect(result).toBe('Hello')
                done()
            })
    })
})
