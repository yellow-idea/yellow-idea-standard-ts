import {LinePushMessage} from '../../src/yellow-sdk-ts/line-api'

describe('LINE Push Message', () => {
    const token = 'ukhaVV4s0UBOaQAh+aOMmSZa6n8kwwX251Vo0k2XIDkU6zpXWHHNoe6XSDDc7dOieYQAN3559gmGPRvNrXAN9HJAoGhHJLyLXJ7VxCnHkroK19nxfD6ImKCoplAXVGnFa8lmhhuLc7aQbNtiNgqdCgdB04t89/1O/w1cDnyilFU='
    const to = 'Ufa542e298418ed8bb76e9b68b2c59b26'
    const messages = [
        {
            'type': 'text',
            'text': 'หิวข้าว',
            'sender': {
                'name': 'Sammy',
                'iconUrl': 'https://stickershop.line-scdn.net/stickershop/v1/sticker/52002749/iPhone/sticker_key@2x.png'
            }
        }
    ]
    it('1 Person 1 Message', async () => {
        const {status} = await LinePushMessage(token, to, messages)
        expect(status).toBe(true)
    })
})
